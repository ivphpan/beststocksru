<?php


namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

class CurrencyController extends Controller
{
    public function actionIndex()
    {
        $content = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp');
        $xml = simplexml_load_string($content);
        $rates = [];
        foreach ($xml->Valute as $item){
            $rates[(string)$item->CharCode] = [
                'name'=>(string)$item->Name, 
                'rate'=>preg_replace('#\,#', '.', $item->Value),
            ];
        }
        if (sizeof($rates)==0){
            return ExitCode::UNAVAILABLE;
        }
        if (\app\models\Currency::find()->count()==0){
            foreach ($rates as $id=>$item) {
                $currency = new \app\models\Currency();
                $currency->id = $id;
                $currency->name = $item['name'];
                $currency->rate = $item['rate'];
                $currency->save();
            }
        } else {
            foreach (\app\models\Currency::find()->batch() as $activeItems) {
                foreach ($activeItems as $activeItem) {
                    if (!empty($rates[$activeItem->id])
                        && $rates[$activeItem->id]['rate']!=$activeItem->rate){
                        $activeItem->rate = $rates[$activeItem->id]['rate'];
                        $activeItem->save();
                    }
                }
            }
        }
        return ExitCode::OK;
    }
}