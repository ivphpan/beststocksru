<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m201111_021210_currency_table
 */
class m201111_021210_currency_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('{{currency}}', [
            'id'=>Schema::TYPE_STRING . ' NOT NULL',
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'rate'=>Schema::TYPE_FLOAT . ' NOT NULL',
        ]);

        $this->addPrimaryKey('', '{{currency}}', ['id']);    
    }

    public function down()
    {
        return $this->dropTable('{{currency}}');
    }

}
