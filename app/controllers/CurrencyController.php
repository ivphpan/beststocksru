<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

class CurrencyController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => \app\models\Currency::find(),
            'pagination' => [
                'pageSize' => 15,
            ]
        ]);
    }

    public function actionView($id)
    {
        return \app\models\Currency::findOne($id);
    }
}