## Тестовое задание для beststocks.ru

Для проверки понадобится docker, docker-compose, git, postman.

### Установка
- Копируем репозиторий **git clone git@bitbucket.org:ivphpan/beststocksru.git**
- Выполняем команду **docker-compose up -d**
- Заходим в контейнер и выполняем команды:
- **docker exec -it beststocksru_php_1 bash** 
- **composer install**
- **chmod 777 /data/www/runtime /data/www/web/assets**
- **./yii migrate --interactive=0**
- **./yii currency**


### Для проверки API используем Postman

#### Для получения списка валют обращаемся по адресу:

- **GET http://localhost:8080/currency**

- Во вкладке авторизации выбираем "Bearer Token", вписываем токен **random-token**

- По умолчанию количество записей 15.

#### Для получение данных со следующей страницы добавляем параметр **page**, например:

- **GET http://localhost:8080/currency?page=2**

#### Для просмотра данных нужной вам валюты используйте следующий адрес:

- **GET http://localhost:8080/currency/\<id\>** например:

- **GET http://localhost:8080/currency/AMD**


### Для проверки обновления курса валют.
- Заходим в контейнер командой **docker exec -it beststocksru_php_1 bash**
- Выполняем команду **./yii currency**